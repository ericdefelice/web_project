# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'DEFAULT USERS'
user1 = User.find_or_create_by_email :name => 'First User', :email => 'user1@example.com', :password => 'password1', :password_confirmation => 'password1'
puts 'user: ' << user1.name

user2 = User.find_or_create_by_email :name => 'Second User', :email => 'user2@example.com', :password => 'password2', :password_confirmation => 'password2'
puts 'user: ' << user2.name